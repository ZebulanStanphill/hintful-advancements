# Changelog

## 2.4.0

### Added

- Add support for snapshots 25w05a through 25w10a (data formats 65-70).

### Fixed

- Make "Kill 'Em with Kindness" lingering potion effect checks actually work (rather than be ignored) on data formats 35-69. I had mistakenly assumed that you could already use data component predicates inside entity predicates, but that only became possible in data format 70.

## 2.3.0

### Added

- Add support for snapshot 25w04a (data format 64).

### Fixed

- Fix obtaining "Kill 'Em with Kindness" via splash potion in data format 35 (24w11a) and newer. (I had mistakenly assumed that thrown potions had been updated to use a standard `contents` slot.)

## 2.2.0

### Added

- Add support for snapshot 25w03a (data format 63).
- Add pack id to `pack.mcmeta` to support tools like [Weld](https://weld.smithed.dev/).

## 2.1.0

### Added

- Declare support for snapshot 25w02a (data format 62).
- Add "Pillar of Smoke" advancement.

## 2.0.0

### Added

- Add "From Flower Buds to Taste Buds" advancement.
- Add support for the following mods:
	- [Colourful Enchanting Tables](https://modrinth.com/mod/colourful-enchanting-tables)
	- [More Enchanting Tables (met)](https://modrinth.com/mod/more-enchanting-tables-(met))
	- [More Pumpkins](https://modrinth.com/mod/more-pumpkin)
	- [Pumpkins Accelerated](https://modrinth.com/mod/more-pumpkins!)
	- [White Pumpkins](https://modrinth.com/mod/white-pumpkins)

### Breaking changes (only affects mod/pack support)

- Namespace all translation keys by prefixing them with `hintful_advancements.`.
- Move tags that were in `hintful_advancements` namespace to new `zeb` namespace intended to standardize general purpose tags for use across all my data packs and mods (and thus simplify compatibility with other mods/packs), whenever a Fabric/Forge/NeoForge common tag (i.e. a tag in the `c` namespace) doesn't already exist.
- `#hintful_advancements:cake_lighter` item tag in particular is now `#zeb:candle_lighter`, because any item that can light a candle would be expected to also light a candle cake.
- `#hintful_advancements:impersistible` entity tag is now `#zeb:nearly_impersistible`, to better reflect its intent. (Vexes aren't truly impersistible; they are only impractical to keep around due to the damage they start taking after a while.)
- Remove `#hintful_advancements:undead` entity tag that was being used in pack formats 32 and earlier, since the earliest format still supported by this pack already includes `#minecraft:undead`.

### Other changes

- Overhaul "Getting Ahead" advancement:
	- Change the advancement criteria:
		- Old: pick up a zombie head.
		- New: have a charged creeper kill a creeper, piglin, skeleton, wither skeleton, or zombie that was first hit by the player.
	- The description has been changed:
		- Old: "Use a charged Creeper to behead a Zombie. Also works on a few other monsters"
		- New: "Hit a Creeper, Piglin, Skeleton, or Zombie; then let a charged Creeper deal the final blow".
- "Frienderman" advancement can now be obtained using any equipment that disguises gazing at an enderman, instead of just the vanilla case of carved pumpkins.
- Change "Face Reveal" advancement description from "Shear a Snow Golem" to "Do you wanna shear a Snow Golem?" because a cute and unintrusive pop culture reference is better than a dry instruction.
- Add `#zeb:enchanting_tables` block tag.
- Add `#zeb:carved_pumpkins` item tag.
- Add `#zeb:drops_head_from_charged_explosion` entity tag.
- Polyfill `#minecraft:gaze_disguise_equipment` for data pack formats 55 and earlier.
- Make pack description translatable.
- Simplify and optimize the implementation of several advancements.

### Removed

- Remove the sound effects for placing bookshelves and enchanting tables in such a way that would power the latter. These audio hints can now be found in the [Hintful Audio Cues data+resource pack](https://modrinth.com/datapack/hintful-audio-cues).

### Fixed

- "Kill 'Em with Kindness" advancement is now obtainable in data format 32 and earlier via all throwable healing potion types. (Typos were causing only the base splash potion case to work.)

## 1.8.0

### Added

- Declare support for 1.21.4 Pre-Release 2 (data format 61).
- Add "Shock Tester" advancement.

### Changed

- Hide "Local Positioning System" advancement and its children until it is obtained.
- Move "Face Reveal" advancement from root of "Adventure" tab to after "Hired Help".
- Change "Face Reveal" icon from carved pumpkin to snow block.
- Exclude mobs from "Persistent Mob" advancement when they can't be made persistent via name tags.
- Improve mod compatibility by implementing "Persistent Mob" advancement using a blacklist of non-name-taggable entities, instead of a whitelist of nameable entities.
- Change "Frienderman" advancement description from "Play “Tag” with an Enderman... without angering it" to "Face an Enderman in a (friendly) game of “tag”", thus clarifying the requirement of staring at the head of the enderman.

## 1.7.0

### Added

- Add "The Cake Is Alight" advancement.

### Changed

- Change "Who Framed Yonder Map-It?" description from "Frame a map for all to see" to "Show off your cartographer's canvas". I felt the description had been somewhat lacking, so I repurposed the old title of the advancement as part of it.

## 1.6.0

### Added

- Declare support for snapshot 24w46a (data format 60).
- Add "Teenage Scutant Ninja Turtles" advancement. (Special thanks to Lime's Advancements for that immaculate title.)

### Changed

- Change "Frienderman" description from "Give an Enderman a stare and a name... without disturbing it" to "Play “Tag” with an Enderman... without angering it". Now it feels a bit more fun and less arbitrary.
- Demote "Still Sea Thru It, You Conduit" advancement from goal to task.
- Demote "Rings of Power" advancement from challenge to goal (and remove experience reward).

## 1.5.0

### Added

- Declare support for snapshot 24w45a (data format 59).
- Add "Rings of Power" advancement for completing a Conduit frame.

### Fixed

- Use updated "X Marks the Spot" description in all supported data pack formats, instead of only format 32 and earlier.

## 1.4.0

### Added

- Declare support for snapshot 24w44a (data format 58).

### Changed

- Rename "You Con-du-it" to "Still Sea Thru It, You Conduit", and change its description from "Activate a submerged Conduit by building a Prismarine frame" to "By Prismarine frame, thy waters to tame". Now it is a rhyme... and yet denser in info.
- Capitalize mob names in "Getting Ahead" advancement description for consistency with vanilla.
- Tidied up implementation of various advancements.

### Fixed

- Fix bug in powerful-bookshelf placement detection.

## 1.3.0

### Added

- Declare support for 1.21.2 Pre-Release 1 (data format 57).

### Changed

- Nametagging a persistent creaking will now count for "Persistent Mob" advancement. (Transient creakings don't count, since they still despawn anyway and can't even be nametagged starting in 1.21.2 Pre-Release 1.)

## 1.2.0

### Added

- Declare support for snapshot 24w40a (data format 56).

### Changed

- Changed "X Marks the Spot" description from "Follow a special Map to find a lost treasure" to "Follow a lost sailor's Map" for brevity and elegance. The new description is based on the description of "Bad Sailors" in [Lime's Advancements](https://github.com/slicedlime/LimesAdvancements).

## 1.1.0

### Added

- Declare support for snapshot 24w39a (data format 55).
- Include `README.md` in built zip files.

### Changed

- Rename "Cartographer's Canvas" to "Who Framed Yonder Map-It?". The old name was generated by Microsoft Copilot; I came up with the new one myself. Although we lose the alliteration and nod to map art, we gain a clever movie reference and a more distinctive title.
- Capitalize "Snow Golem" in "Face Reveal" description for consistency with vanilla advancements and other advancements in this pack.
- Change "Spice Cube" description from "Feed a Frog using a more exotic kind of slime" to "Let a Frog feast on a spicier slime" for terseness, increased alliteration, and a slight hint that you can't force-feed the frog here.
- Change "Kill 'Em with Kindness" description from "Hurt an undead creature with a splash potion" to "Hurt an undead creature via Splash Potion" for terseness and consistent capitalization.
- Change "Multi-Book Structure" description from "Level up your Enchanting Table by placing certain blocks around it" to "Level up your Enchanting Table by ensquaring it with knowledge" for brevity and poetry. (Is "ensquaring" a word? It is now.)

## 1.0.0

- Initial release.
