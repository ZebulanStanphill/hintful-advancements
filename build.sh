#!/usr/bin/env sh

if ! command -v 7za >/dev/null 2>&1; then
	echo >&2 "7za must be installed to use this script. https://linux.die.net/man/1/7za"
	exit 1
fi

# If we don't do this, the contents of the old zip file will be included in the new one.
rm hintful_advancements.zip

# We have to do this to prevent the data_pack folder itself from being included in the zip.
cd data_pack

7za a -tzip -mx=9 ../hintful_advancements.zip * ../CHANGELOG.md ../LICENSE.txt ../README.md
