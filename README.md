# 💡 Hintful Advancements

## ❓ Description

Advancements and the recipe book have done much to guide players into discovering Minecraft mechanics, yet much remains nigh-unfindable within the game itself. Powering enchanting tables, activating conduits, marking banners on a map... how is a player (unaided by external sources) expected to discover such features?

This data pack fills in the blanks with 20+ new advancements focused on features left obscure by the base game. Each advancement is carefully crafted to reveal new possibilities, while not spoon-feeding every detail. Beyond mere tutorialization, this pack aims to encourage exploration and experimentation. In keeping with vanilla, the icon, title, and description of each advancement are deliberately chosen to concisely deliver both hints and humor.

Supports all Minecraft Java Edition versions from 1.20.3 snapshot 23w41a through 1.21.5 snapshot 25w10a.

If you like this pack, you may also enjoy [Hintful Audio Cues](https://modrinth.com/datapack/hintful-audio-cues).

## ➕ Added Advancements

<details>
<summary>Advancement spoilers</summary>

### 🌳 Minecraft

- 📚 "Multi-Book Structure" (level up enchanting table)

### 🧭 Adventure

- ☁️ "Pillar of Smoke" (use campfire to create a lot of smoke)
- 🔱 "Shock Tester" (try every lightning-based mob transformation)
- 💀 "Getting Ahead" (hit certain mobs, then have a charged creeper deal the final blow)
- ☃️ "Face Reveal" (shear a snow golem)
- 🤖 "Iron Supplements" (repair an iron golem)
- 🗺️ "Local Positioning System" (get a map)
- 🪪 "Persistent Mob" (name a mob)
- 📍 "Point of Interest" (mark a named banner on a map)
- 🖼️ "Who Framed Yonder Map-It?" (frame a map)
- ❌ "X Marks the Spot" (find buried treasure)
- 🫧 "Still Sea Thru It, You Conduit" (activate a conduit)
- 🛟 "Rings of Power" (complete a conduit frame)

### 👨‍🌾 Husbandry

- 🍄‍🟫 "From Flower Buds to Taste Buds" (get suspicious stew from a special bovine)
- 🎂 "The Cake Is Alight" (light a cake)
- 🐢 "Teenage Scutant Ninja Turtles" (get a turtle shell)

### 🔥 Nether

- 🛏️ "Beds Away!" (sleep in the Nether)
- ❤️ "Kill 'Em with Kindness" (attack the undead with healing potions)
- 🌋 "Spice Cube" (get a froglight)

### 🕳️ The End

- 🎃 "Frienderman" (name an enderman without triggering it)
- 🚀 "Rocket Science" (fly via elytra above the highest blocks)

</details>

## Mod Compatibility

This pack is designed with mods in mind; many advancements are implemented using tags rather than hardcoded block/item/entity lists. The [standardized common tags](https://wiki.fabricmc.net/community:common_tags) are used whenever possible; and for things not covered by those, tags in my own `zeb` namespace are used for anything not specific to this data pack.

Built-in support is provided for the following mods:

- [Colourful Enchanting Tables](https://modrinth.com/mod/colourful-enchanting-tables)
- [More Enchanting Tables (met)](https://modrinth.com/mod/more-enchanting-tables-(met))
- [More Pumpkins](https://modrinth.com/mod/more-pumpkin)
- [Pumpkins Accelerated](https://modrinth.com/mod/more-pumpkins!)
- [White Pumpkins](https://modrinth.com/mod/white-pumpkins)

If you spot any places where compatibility with another mod or pack could be improved, let me know on [GitLab](https://gitlab.com/ZebulanStanphill/hintful-advancements/-/issues).

If you're a mod/pack developer wanting to add/remove advancement criteria (e.g. adding another mob to the list for "Shock Tester"), consider using [Patched](https://modrinth.com/mod/patched).

## 🤔 Developer Commentary

If you're curious about my opinions and reasoning regarding which advancements I chose to implement and how I implemented them, check out [this document on the GitLab](https://gitlab.com/ZebulanStanphill/hintful-advancements/-/blob/master/developer-commentary.md).

## 💬 Bugs, Suggestions, and Feedback

Found a bug? Have an idea to improve the pack? Let me know by opening an issue on the [GitLab](https://gitlab.com/ZebulanStanphill/hintful-advancements/-/issues).

## 🏅 Credits

### Advancement Titles, Descriptions, and Ideas

"Point of Interest": Banner pattern yoinked from [Lime's Advancements](https://github.com/slicedlime/LimesAdvancements/) by [slicedlime](https://www.youtube.com/slicedlime); title was actually just coincidentally the same, though.

"Who Framed Yonder Map-It?": Inspired by "Map Wall" from Lime's Advancements, but now it has a funnier name.

"X Marks the Spot": Title taken from Lime's Advancements, and the description is derived from the description of the "Bad Sailors" advancement in that pack.

"Still Sea Thru It, You Conduit": Title inspired by "You Con-du-it" from Lime's Advancements.

"Face Reveal": Title taken from similar advancement in Lime's Advancements.

"Getting Ahead": Title taken from Lime's Advancements.

"Teenage Scutant Ninja Turtles": Title taken from Lime's Advancements.

### Other

Ray-casting logic based on [Raycasting Engine/Framework](https://www.planetminecraft.com/data-pack/raycasting-engine-framework/) by [itskegnh](https://www.planetminecraft.com/member/itskegnh/).

Shout out to Rutherford from the ["Minecraft Commands" Discord](https://discord.gg/9wNcfsH), who suggested a fix for false positives when interacting with allays for the "Persistent Mob" advancement.

Shout out to PiroPito, whose [blind playthrough of Minecraft](https://www.youtube.com/playlist?list=PLbqkLu2V1bJJUQ2aLZjFdz8decGs1kHg-) was a big inspiration for making this pack.

Props to everyone at Mojang who works or previously worked on Minecraft and brought the in-game tutorialization to where it is now.

And of course, a very special thanks to the Lord God, without whom none of this would have been possible.

## 👨‍⚖️ License

I'm not even sure if the contents of this data pack are copyrightable, but insofar as they are, see [`LICENSE.txt`](https://gitlab.com/ZebulanStanphill/hintful-advancements/-/blob/master/LICENSE.txt).
