# Ensure this technical advancement can be triggered again.
# We can't use @s since scheduled functions lack context; thus, as an unfortunate side-effect, it is possible for the advancement to be revoked too early if multiple players are milking brown mooshrooms at around the same time.
# Trying to fix this would, I think, add too much additional complexity. The player can simply milk the mooshroom again if he didn't get the advancement the first time.
advancement revoke @a only hintful_advancements:technical/use_bowl_on_brown_mooshroom
