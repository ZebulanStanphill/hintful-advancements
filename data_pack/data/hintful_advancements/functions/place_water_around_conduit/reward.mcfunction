# Ensure this technical advancement can be triggered again.
advancement revoke @s only hintful_advancements:technical/place_water_around_conduit

# If player doesn't already have complete_conduit_frame advancement, find the water (or waterlogged block) that was just placed and grant the advancement if appropriate.
execute if entity @s[advancements={hintful_advancements:adventure/complete_conduit_frame=false}] \
	anchored eyes run \
	function hintful_advancements:raycast_to_function_match { \
		is_match: "hintful_advancements:place_water_around_conduit/is_match", \
		max_distance: 8, \
		on_match: "hintful_advancements:place_water_around_conduit/on_match" \
	}
