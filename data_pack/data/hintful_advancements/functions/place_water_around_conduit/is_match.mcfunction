# Determine if the placed block is part of a complete conduit frame structure.
execute positioned ~1 ~ ~ if predicate hintful_advancements:is_conduit_in_complete_frame run return 1
execute positioned ~ ~1 ~ if predicate hintful_advancements:is_conduit_in_complete_frame run return 1
execute positioned ~ ~ ~1 if predicate hintful_advancements:is_conduit_in_complete_frame run return 1

execute positioned -1 ~ ~ if predicate hintful_advancements:is_conduit_in_complete_frame run return 1
execute positioned ~ ~-1 ~ if predicate hintful_advancements:is_conduit_in_complete_frame run return 1
execute positioned ~ ~ ~-1 if predicate hintful_advancements:is_conduit_in_complete_frame run return 1

execute positioned ~1 ~1 ~ if predicate hintful_advancements:is_conduit_in_complete_frame run return 1
execute positioned ~1 ~ ~1 if predicate hintful_advancements:is_conduit_in_complete_frame run return 1
execute positioned ~ ~1 ~1 if predicate hintful_advancements:is_conduit_in_complete_frame run return 1

execute positioned ~-1 ~-1 ~ if predicate hintful_advancements:is_conduit_in_complete_frame run return 1
execute positioned ~-1 ~ ~-1 if predicate hintful_advancements:is_conduit_in_complete_frame run return 1
execute positioned ~ ~-1 ~-1 if predicate hintful_advancements:is_conduit_in_complete_frame run return 1

execute positioned ~1 ~-1 ~ if predicate hintful_advancements:is_conduit_in_complete_frame run return 1
execute positioned ~1 ~ ~-1 if predicate hintful_advancements:is_conduit_in_complete_frame run return 1
execute positioned ~ ~1 ~-1 if predicate hintful_advancements:is_conduit_in_complete_frame run return 1
execute positioned ~-1 ~1 ~ if predicate hintful_advancements:is_conduit_in_complete_frame run return 1
execute positioned ~-1 ~ ~1 if predicate hintful_advancements:is_conduit_in_complete_frame run return 1
execute positioned ~ ~-1 ~1 if predicate hintful_advancements:is_conduit_in_complete_frame run return 1

execute positioned ~1 ~1 ~1 if predicate hintful_advancements:is_conduit_in_complete_frame run return 1
execute positioned ~-1 ~-1 ~-1 if predicate hintful_advancements:is_conduit_in_complete_frame run return 1

execute positioned ~1 ~1 ~-1 if predicate hintful_advancements:is_conduit_in_complete_frame run return 1
execute positioned ~1 ~-1 ~1 if predicate hintful_advancements:is_conduit_in_complete_frame run return 1
execute positioned ~1 ~-1 ~-1 if predicate hintful_advancements:is_conduit_in_complete_frame run return 1
execute positioned ~-1 ~1 ~1 if predicate hintful_advancements:is_conduit_in_complete_frame run return 1
execute positioned ~-1 ~1 ~-1 if predicate hintful_advancements:is_conduit_in_complete_frame run return 1
execute positioned ~-1 ~-1 ~1 if predicate hintful_advancements:is_conduit_in_complete_frame run return 1

# No fully-powered conduit found. (Not using return fail since it isn't supported before data format 23.)
return 0
