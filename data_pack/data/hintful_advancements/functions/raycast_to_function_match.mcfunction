# This function casts a ray that steps forward until either a provided match condition returns a successful value, or else a specified max distance is reached. Upon finding a match, a provided callback is run.
# Ray-casting originally based on https://www.planetminecraft.com/data-pack/raycasting-engine-framework/, but revised to take advantage of newer syntax.

# This is where we detect if the ray has hit the desired target, and if so, we run the on_match function, and return if it succeeds.
$execute align xyz if function $(is_match) run return run function $(on_match)

# Target not found, so if the ray hasn't traveled too far, move the ray a bit further and return the result of running this function again.
$execute positioned ^ ^ ^.02 if entity @s[distance=..$(max_distance)] run return run function hintful_advancements:raycast_to_function_match {is_match: "$(is_match)", max_distance: $(max_distance), on_match: "$(on_match)"}

# Nothing found within max distance, so return zero value. (Not using return fail since it isn't supported before data format 23.)
return 0
