# Ensure this technical advancement can be triggered again.
advancement revoke @s only hintful_advancements:technical/place_conduit_frame_block

# If player doesn't already have complete_conduit_frame advancement, find the block that was just placed and grant the advancement if appropriate.
execute if entity @s[advancements={hintful_advancements:adventure/complete_conduit_frame=false}] \
	anchored eyes run \
	function hintful_advancements:raycast_to_block { \
		block: "#zeb:conduit_frame_blocks", \
		max_distance: 8, \
		on_match: "hintful_advancements:place_conduit_frame_block/on_found_placed_block" \
	}
