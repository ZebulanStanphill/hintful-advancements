# Ensure this technical advancement can be triggered again.
advancement revoke @s only hintful_advancements:technical/use_map_on_banner

# If player doesn't already have use_map_on_named_banner advancement, find the banner that was just interacted with and grant the advancement if appropriate.
execute if entity @s[advancements={hintful_advancements:adventure/use_map_on_named_banner=false}] \
	anchored eyes run \
	function hintful_advancements:raycast_to_block { \
		block: "#minecraft:banners", \
		max_distance: 8, \
		on_match: "hintful_advancements:use_map_on_banner/on_found_banner" \
	}
