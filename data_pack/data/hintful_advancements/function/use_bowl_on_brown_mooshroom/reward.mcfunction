# If the player is receiving a suspicious stew from the mooshroom, that won't occur until the next tick after the interaction. Therefore, delay revoking this technical advancement until after the get_suspicious_stew_from_mooshroom advancement has a chance to be triggered.
schedule function hintful_advancements:use_bowl_on_brown_mooshroom/revoke 2t append
