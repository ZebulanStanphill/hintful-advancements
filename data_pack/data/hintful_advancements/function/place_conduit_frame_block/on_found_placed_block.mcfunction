# If the placed block is part of a complete conduit frame structure, grant the complete_conduit_frame advancement and return.
execute positioned ~2 ~ ~ if predicate hintful_advancements:is_conduit_in_complete_frame run return run advancement grant @s only hintful_advancements:adventure/complete_conduit_frame
execute positioned ~ ~2 ~ if predicate hintful_advancements:is_conduit_in_complete_frame run return run advancement grant @s only hintful_advancements:adventure/complete_conduit_frame
execute positioned ~ ~ ~2 if predicate hintful_advancements:is_conduit_in_complete_frame run return run advancement grant @s only hintful_advancements:adventure/complete_conduit_frame

execute positioned ~-2 ~ ~ if predicate hintful_advancements:is_conduit_in_complete_frame run return run advancement grant @s only hintful_advancements:adventure/complete_conduit_frame
execute positioned ~ ~-2 ~ if predicate hintful_advancements:is_conduit_in_complete_frame run return run advancement grant @s only hintful_advancements:adventure/complete_conduit_frame
execute positioned ~ ~ ~-2 if predicate hintful_advancements:is_conduit_in_complete_frame run return run advancement grant @s only hintful_advancements:adventure/complete_conduit_frame

execute positioned ~2 ~2 ~ if predicate hintful_advancements:is_conduit_in_complete_frame run return run advancement grant @s only hintful_advancements:adventure/complete_conduit_frame
execute positioned ~2 ~ ~2 if predicate hintful_advancements:is_conduit_in_complete_frame run return run advancement grant @s only hintful_advancements:adventure/complete_conduit_frame
execute positioned ~ ~2 ~2 if predicate hintful_advancements:is_conduit_in_complete_frame run return run advancement grant @s only hintful_advancements:adventure/complete_conduit_frame

execute positioned ~-2 ~-2 ~ if predicate hintful_advancements:is_conduit_in_complete_frame run return run advancement grant @s only hintful_advancements:adventure/complete_conduit_frame
execute positioned ~-2 ~ ~-2 if predicate hintful_advancements:is_conduit_in_complete_frame run return run advancement grant @s only hintful_advancements:adventure/complete_conduit_frame
execute positioned ~ ~-2 ~-2 if predicate hintful_advancements:is_conduit_in_complete_frame run return run advancement grant @s only hintful_advancements:adventure/complete_conduit_frame

execute positioned ~2 ~1 ~ if predicate hintful_advancements:is_conduit_in_complete_frame run return run advancement grant @s only hintful_advancements:adventure/complete_conduit_frame
execute positioned ~2 ~ ~1 if predicate hintful_advancements:is_conduit_in_complete_frame run return run advancement grant @s only hintful_advancements:adventure/complete_conduit_frame
execute positioned ~ ~2 ~1 if predicate hintful_advancements:is_conduit_in_complete_frame run return run advancement grant @s only hintful_advancements:adventure/complete_conduit_frame

execute positioned ~1 ~2 ~ if predicate hintful_advancements:is_conduit_in_complete_frame run return run advancement grant @s only hintful_advancements:adventure/complete_conduit_frame
execute positioned ~1 ~ ~2 if predicate hintful_advancements:is_conduit_in_complete_frame run return run advancement grant @s only hintful_advancements:adventure/complete_conduit_frame
execute positioned ~ ~1 ~2 if predicate hintful_advancements:is_conduit_in_complete_frame run return run advancement grant @s only hintful_advancements:adventure/complete_conduit_frame

execute positioned ~-2 ~-1 ~ if predicate hintful_advancements:is_conduit_in_complete_frame run return run advancement grant @s only hintful_advancements:adventure/complete_conduit_frame
execute positioned ~-2 ~ ~-1 if predicate hintful_advancements:is_conduit_in_complete_frame run return run advancement grant @s only hintful_advancements:adventure/complete_conduit_frame
execute positioned ~ ~-2 ~-1 if predicate hintful_advancements:is_conduit_in_complete_frame run return run advancement grant @s only hintful_advancements:adventure/complete_conduit_frame

execute positioned ~-1 ~-2 ~ if predicate hintful_advancements:is_conduit_in_complete_frame run return run advancement grant @s only hintful_advancements:adventure/complete_conduit_frame
execute positioned ~-1 ~ ~-2 if predicate hintful_advancements:is_conduit_in_complete_frame run return run advancement grant @s only hintful_advancements:adventure/complete_conduit_frame
execute positioned ~ ~-1 ~-2 if predicate hintful_advancements:is_conduit_in_complete_frame run return run advancement grant @s only hintful_advancements:adventure/complete_conduit_frame

execute positioned ~-2 ~2 ~ if predicate hintful_advancements:is_conduit_in_complete_frame run return run advancement grant @s only hintful_advancements:adventure/complete_conduit_frame
execute positioned ~-2 ~ ~2 if predicate hintful_advancements:is_conduit_in_complete_frame run return run advancement grant @s only hintful_advancements:adventure/complete_conduit_frame
execute positioned ~ ~-2 ~2 if predicate hintful_advancements:is_conduit_in_complete_frame run return run advancement grant @s only hintful_advancements:adventure/complete_conduit_frame

execute positioned ~-2 ~1 ~ if predicate hintful_advancements:is_conduit_in_complete_frame run return run advancement grant @s only hintful_advancements:adventure/complete_conduit_frame
execute positioned ~-2 ~ ~1 if predicate hintful_advancements:is_conduit_in_complete_frame run return run advancement grant @s only hintful_advancements:adventure/complete_conduit_frame
execute positioned ~ ~-2 ~1 if predicate hintful_advancements:is_conduit_in_complete_frame run return run advancement grant @s only hintful_advancements:adventure/complete_conduit_frame

execute positioned ~-1 ~2 ~ if predicate hintful_advancements:is_conduit_in_complete_frame run return run advancement grant @s only hintful_advancements:adventure/complete_conduit_frame
execute positioned ~-1 ~ ~2 if predicate hintful_advancements:is_conduit_in_complete_frame run return run advancement grant @s only hintful_advancements:adventure/complete_conduit_frame
execute positioned ~ ~-1 ~2 if predicate hintful_advancements:is_conduit_in_complete_frame run return run advancement grant @s only hintful_advancements:adventure/complete_conduit_frame

execute positioned ~2 ~-2 ~ if predicate hintful_advancements:is_conduit_in_complete_frame run return run advancement grant @s only hintful_advancements:adventure/complete_conduit_frame
execute positioned ~2 ~ ~-2 if predicate hintful_advancements:is_conduit_in_complete_frame run return run advancement grant @s only hintful_advancements:adventure/complete_conduit_frame
execute positioned ~ ~2 ~-2 if predicate hintful_advancements:is_conduit_in_complete_frame run return run advancement grant @s only hintful_advancements:adventure/complete_conduit_frame

execute positioned ~2 ~-1 ~ if predicate hintful_advancements:is_conduit_in_complete_frame run return run advancement grant @s only hintful_advancements:adventure/complete_conduit_frame
execute positioned ~2 ~ ~-1 if predicate hintful_advancements:is_conduit_in_complete_frame run return run advancement grant @s only hintful_advancements:adventure/complete_conduit_frame
execute positioned ~ ~2 ~-1 if predicate hintful_advancements:is_conduit_in_complete_frame run return run advancement grant @s only hintful_advancements:adventure/complete_conduit_frame

execute positioned ~1 ~-2 ~ if predicate hintful_advancements:is_conduit_in_complete_frame run return run advancement grant @s only hintful_advancements:adventure/complete_conduit_frame
execute positioned ~1 ~ ~-2 if predicate hintful_advancements:is_conduit_in_complete_frame run return run advancement grant @s only hintful_advancements:adventure/complete_conduit_frame
execute positioned ~ ~1 ~-2 if predicate hintful_advancements:is_conduit_in_complete_frame run return run advancement grant @s only hintful_advancements:adventure/complete_conduit_frame

# No fully-powered conduit found. (Not using return fail since it isn't supported until data format 23.)
return 0
