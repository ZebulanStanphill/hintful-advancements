# Ray-casting originally based on https://www.planetminecraft.com/data-pack/raycasting-engine-framework/, but revised to take advantage of newer syntax.

# This is where we detect if the ray has hit the desired target, and if so, we run the success function and return.
$execute align xyz if block ~ ~ ~ $(block) run return run function $(on_match)

# Target not found, so if the ray hasn't traveled too far, move the ray a bit further and return the result of running this function again.
$execute positioned ^ ^ ^.02 if entity @s[distance=..$(max_distance)] run return run function hintful_advancements:raycast_to_block {block: "$(block)", max_distance: $(max_distance), on_match: "$(on_match)"}

# Nothing found within max distance, so return zero value. (Not using return fail since it isn't supported until data format 23.)
return 0
