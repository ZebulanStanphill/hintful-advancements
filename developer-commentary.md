# 🤔 Developer Commentary

Note that this document contains many detailed spoilers for the advancements.

## General

I am of the opinion that a game ought to teach most of its mechanics without relying on external wikis. Yet I consider a typical hand-holding tutorial boring and sometimes even overwhelming. I think Minecraft (and Java Edition specifically) handles its tutorialization quite well nowadays, thanks to the introduction of certain structures like ruined portals, the improved recipe system of 1.12+, and the advancements system. However, several features remain excessively difficult to discover solely through what the game itself provides.

This pack is not intended to add any advancements for their own sake, but rather to reveal every otherwise obscure yet useful mechanic in the vanilla game. However, that doesn't mean the advancements should be dry info dumps; I've attempted to double-down on the playfulness of the vanilla advancements. A little levity and the right amount of wit go a long way toward transforming sterile tutorialization into something a player can look forward to. I believe that, where possible, advancements should not only teach the player, but even encourage him to experiment on his own. Minecraft is a sandbox, so its suggested goals should inspire the player, rather than merely hand him a to-do list.

## Specific Advancements

### 🌳 Minecraft

#### 📚 "Multi-Book Structure"

The obscurity (and peculiarity) of enchanting table mechanics was one of the motivating factors that led me to create this data pack. Leaving such an important part of the game so undiscoverable is absurd.

Writing the logic to detect the relevant block placements proved somewhat challenging at first, but later merely time-consuming.

When this pack first released, there was a sound effect added for placing a bookshelf or enchantment table in such a way that would power the enchanting table. From 2.0.0 onward, this has been moved to a separate data+resource pack called [Hintful Audio Cues](https://modrinth.com/datapack/hintful-audio-cues).

### 🧭 Adventure

#### ☁️ "Pillar of Smoke"

Smoke-producing campfires serve only an aesthetic purpose (in vanilla), but in a game with an emphasis on building, aesthetics are an especially relevant matter. This particular mechanic makes some sense in hindsight, but who would have the foresight to find it?

#### 🔱 "Shock Tester"

This one is intended to teach the player about charged creepers and brown mooshrooms. The former is required knowledge for "Getting Ahead", and the other is required knowledge for "From Flower Buds to Taste Buds" (unless you get lucky with breeding or thunderstorms).

I think conceptually, it's funny to have this advancement in the game as a counterpart to "Surge Protector".

The special behavior for lightning striking turtles is excluded from this advancement because I don't think it really counts, and it's not particularly useful anyway.

#### 💀 "Getting Ahead"

"Mind-Blowing" was my initial idea for the title, but [slicedlime's pun](https://github.com/slicedlime/LimesAdvancements/blob/master/data/lap/advancements/ocean/mob_head.json) seemed superior.

Ideally, the advancement would not require the player to hit the mob before it is beheaded, and it would be sufficient for a player to be near a charged creeper when it kills one of the relevant mobs. However, there is no advancement trigger to detect this, and I wish to avoid using tick functions in this data pack as much as possible.

Before version 2.0.0, this advancement was obtained by picking up a zombie head. I had limited the criteria to zombie heads for 2 reasons:

- Skeleton skulls are obtainable in ancient cities, and wither skeleton skulls are obtainable as a standard mob drop. There is no practical way of determining how a player obtained heads of this type.
- Including zombies, creepers, and piglins, but not skeletons felt weird, so I figured I should pick just one. I chose zombies since they were common and slow-moving.

It later occurred to me, however, that when using popular mods and/or data packs that add head drops via other means, the advancement became trivial and essentially unrelated to the trident advancements preceding it.

I eventually decided to rework the advancement to actually require a mob to die via a charged creeper explosion. The most practical way to implement this was via the `player_killed_entity` trigger, which requires the player to deal some damage to the mob before its beheading. I think it's a fine trade-off, however, since it provides a few benefits:

- The player can now choose from a larger set of victims.
- The advancement now requires the player to actually be involved in the challenge, whereas before the player could simply be handed a head obtained by another player.
- Combined with its reworded description, the advancement now retains a sense of purpose even in contexts where mob head drop mechanics are altered/removed.

#### ☃️ "Face Reveal"

Title taken from [similar advancement in Lime's Advancements](https://github.com/slicedlime/LimesAdvancements/blob/master/data/lap/advancements/adventure/shear_golem.json). My pack intentionally lacks an equivalent to the preceding ["Snowball Fight" advancement](https://github.com/slicedlime/LimesAdvancements/blob/master/data/lap/advancements/adventure/summon_snow_golem.json) from that pack, since I prefer to convey more info with fewer advancements.

My hope is that, if a player can figure out how to construct an iron golem (which PiroPito proved to be possible), a snow golem will be comparatively easy to discover afterward. Indeed, revealing the existence of the snow golem is presently the true purpose of this advancement, since the shearing-pumpkin mechanic is somewhat less obscure today thanks to the introduction of a similar intraction with wolf armor along with the vanilla "Shear Brilliance" advancement.

The use of a snow block as the icon is intended to make it more obvious that snow golems are created from snow blocks, since otherwise I suspect some players would mistakenly assume they're created by altering iron golems.

#### 🤖 "Iron Supplements"

How else are players going to know they can repair iron golems using ingots? I'm quite pleased with the title I thought up for this one.

#### 🪪 "Persistent Mob"

Mob despawning and its prevention are something that would be quite difficult to discover and experiment with, unguided by external sources. The description of this advancement is thus rather literal, though I did attempt to tack some levity onto it.

#### 🗺️ "Local Positioning System"

Of all the advancements in the pack, this is the one I consider least essential. It exists almost exclusively to group several other more important advancements that would otherwise be scatted throughout the "Adventure" tab. I do quite like the title I came up with, though.

Since crafting and exploration will eventually lead the player to using a map anyway, I decided it would be cool to hide this advancement (and thus also its children) until it is obtained. This keeps the "Adventure" tab a bit more focused early on, and delays the reveal of the conduit.

#### 📍 "Point of Interest"

Funnily enough, I actually independently came up with the same advancement title as [Lime's Advancements](https://github.com/slicedlime/LimesAdvancements/blob/master/data/lap/advancements/adventure/map_marker.json) before discovering it, and I coincidentally was also already using a blue banner for the icon, albeit without the pattern (which I promptly yoinked).

#### 🖼️ "Who Framed Yonder Map-It?"

Inspired by ["Map Wall" from Lime's Advancements](https://github.com/slicedlime/LimesAdvancements/blob/master/data/lap/advancements/adventure/framed_map.json), this advancement is intended to guide players into discovering the special appearance of maps in item frames. The description also attempts to inspire players by hinting at the idea of map art.

I initially titled this advancement "Cartographer's Canvas" (a Microsoft Copilot-generated alliteration), which I felt was more poetic, albeit at the cost of losing the nod to playerbase lingo that slicedlime's advancement had. I grew unsatisfied with my chosen title, however, since I realized that even an unframed map could be called a "cartographer's canvas". I then came up with the current title, which is more descriptive of the actual advancement criteria, while also incorporating a cute (if somewhat strained) pop culture reference rhyme. I later realized I could improve the description as well by incorporating the previous title into it.

#### ❌ "X Marks the Spot"

The purpose of this advancement is to suggest the existence of shipwrecks and buried treasure maps, while also indicating (by the icon) that there is something unique to be found among the treasure. Since there are multiple chests in shipwrecks, yet only one contains the relevant map, a player might miss the map the first time and thus not realize the significance of the structure. That's the main reason I included this advancement, though I could see myself changing my mind and removing this advancement in the future.

The title is [taken from Lime's Advancements](https://github.com/slicedlime/LimesAdvancements/blob/master/data/lap/advancements/ocean/heart_of_the_sea.json), and the description is derived from the description of the ["Bad Sailors" advancement](https://github.com/slicedlime/LimesAdvancements/blob/master/data/lap/advancements/ocean/treasure_map.json) in that pack.

#### 🫧 "Still Sea Thru It, You Conduit"

Title inspired by "You Con-du-it" from Lime's Advancements. The lack of dashes is mainly because I was trying to stop the title from getting any longer. My hope is that the rhymes in the title and description will serve as a reasonable riddle that can subtly reveal multiple aspects of the conduit frame structure:

- The purpose of the conduit ("still"-ing i.e. calming the sea, and "thy waters to tame").
- The need for water around the conduit ("still sea thru it").
- The blocks used to construct the frame ("by prismarine frame").

I don't think these hints are sufficient on their own, but that's where the following advancement (which becomes visible soon after this one) comes in.

#### 🛟 "Rings of Power"

This is essentially a part two for "Still Sea Thru It, You Conduit". Its title and description reveal further details about the conduit frame structure:

- It incorporates ring-esque shapes.
- It uses hollow squares related to the three dimensional axes.
- It involves perpendicular angles.

Implementing this advancement was quite time-consuming, with some heavy use of the offset properties of location predicates. I would've gone even further and had the whole thing use predicates without any functions, but since location predicates can't be nested with an offset, that would've required a lot of needless repitition.

I would've had only a single advancement for conduits, but the structure is so complex that I figured several lines of text would be needed to guide the player toward discovering it, and I don't want any single advancement to become overly verbose.

I think perhaps the best solution to teaching players the conduit frame design would be to add a painting, similar to what Mojang did for the wither. But I'm a coder, not a pixel artist.

### 👨‍🌾 Husbandry

#### 🍄‍🟫 "From Flower Buds to Taste Buds"

This advancement serves to teach the player about milking mooshrooms with bowls, and specifically the unique behavior of brown mooshrooms, the latter of which I cannot fathom players ever discovering on their own.

I'd been wanting to add an advancement for this before I even officially released the first version of the pack. The main obstacles were:

1. Implementing it in a way that covered most cases without being messy or unperformant.
2. Figuring out a title and description that felt clever and descriptive, without spoiling too much all at once.

After how neat the conduit advancement titles and descriptions turned out, I wanted to use the riddle-rhyme forumla again. In particular, I wanted something that would not explicitly reveal the existence of mooshrooms, yet would clearly point to them once the player had discovered them.

I considered naming the advancement "Flower Buds, Taste Buds, and Bovine Buds", but I decided that was a tad too long (both visually and orally) and that the incorporation of "from" and "to" into the alliteration of the final title was plenty poetic (while also helping to reinforce the input-output aspect of the interaction).

#### 🎂 "The Cake Is Alight"

Since Minecraft generally avoids the concept of having blocks be placed within the location of another block, an uninformed player may not even think to consider that candle cakes could exist. That is one of the reasons for this advancement.

The other reason is that I wanted to bring back the challenge of the old "The Lie" achievement from Java Edition 1.11 and older versions, which required obtaining multiple ingredients tied to several distinct game mechanics. Adding a candle on top of all that (and having to light it) further enhances the cake as a celebratory symbol of player progression, while also placing a fun twist on the classic meme referenced in the original achievement.

#### 🐢 "Teenage Scutant Ninja Turtles"

I love the title of the [similar advancement in Lime's Advancements](https://github.com/slicedlime/LimesAdvancements/blob/master/data/lap/advancements/ocean/scute.json). It has such poetic depth and thematic relevance.

The purpose of this advancement is twofold:

- I wanted to reveal the existence of the turtle shell. Turtle scutes are relatively difficult to discover, and it's easy to imagine a player never being around to see one, even if he successfully breeds turtles and hatches their eggs. And of course, obtaining a turtle scute is what reveals the turtle shell recipe.
- I wanted to hint at the purpose of the turtle shell to incentivize the player to interact with turtles.

The increased emphasis on the turtle shell specifically is why its crafting is the advancement trigger, rather than the obtaining of a single scute (as is the case in Lime's Advancements). Having to successfully raise multiple turtles also raises the challenge of the advancement, thus helping to justify its experience reward. In general, I prefer condensing multiple steps toward a goal into a single advancement, so that the importance of each advancement is maintained, and so I can avoid needless crowding of the already well-filled advancements screen.

The placement of this advancement after "The Parrots and the Bats" is intended to emphasize the relevance of turtle breeding to the task.

The idea of protecting and raising turtles (with combat possibly involved) happens to fit quite well with the advancement title, if you think about it.

### 🔥 Nether

#### 🛏️ "Beds Away!"

The behavior of beds in the nether is rather surprising, and perhaps a little unfair. I've attempted to carefully word the title and description such that the attentive player can escape an unsavory fate, without immediately spoiling all the fun.

The title, of course, can be taken both literally and as a pun.

#### ❤️ "Kill 'Em with Kindness"

Up until the relatively recent addition of Trial Chambers, there were actually no naturally-occuring splash potions in the game, so this advancement serves as an indicator that they exist, in addition to specifically suggesting the inverted nature of the healing and harming effects on the undead.

#### 🌋 "Spice Cube"

The cancelation of fireflies resulted in froglights being locked behind an interaction that I can't imagine most people ever figuring out on their own, even if the frog-slime interaction already subtly suggests it. Still, I tried not to make the answer overbearingly obvious in the title and description of this advancement.

### 🕳️ The End

#### 🎃 "Frienderman"

Yes, the title is a Story Mode reference, though of course it works well even without that context.

The specific criteria came about from trying to find a way to detect a player looking at an enderman without resorting to polling on every tick.

I might move this advancement into the Adventure tab at some point, since it's not truly End-exclusive; it could work well as a follow-up to "Persistent Mob".

#### 🚀 "Rocket Science"

I chose not to use the `is_flying` flag in "Rocket Science" because I want to specifically target elytra flight and ignore creative mode flight. The `FallFlying` NBT tag remains the most foolproof way to accomplish this. (Mojang, please add `is_gliding`.)

I also intentionally chose not to check for firework usage in "Rocket Science", so that a player may also obtain the advancement through more creative (and less obvious) means, e.g. TNT launchers.

## Tags

The meaning and purpose for most of the tags defined in this data pack are self-explanatory. But for the rest...

`#zeb:bad_respawn_point` (damage type) exists only because I couldn't reference the `minecraft:bad_respawn_point` damage type directly in the advancement criteria.

`#zeb:drops_head_from_charged_explosion` is a list of every mob that drops its head when killed by a charged creeper explosion, regardless of whether it can also drop its head via other means.

`#zeb:nearly_impersistible` is a list of mobs that would not effectively be made persistent by naming them via a name tag. For example: the wandering trader despawns anyway, and vexes start automatically taking damage after a while.

`#zeb:not_name_taggable` is a list of entities that cannot be given a name via a name tag. Some entities on this list can be given names through other means, e.g. item renames via an anvil. This tag exists mostly because I lack a simple way to determine if a player interacting with an entity while holding a name tag actually involved naming the entity; right-clicking a boat while holding a name tag just puts you inside the boat, for example.

`#zeb:carved_pumpkins` is currently only used in data formats before 56 (24w40a), where the tag is used to polyfill `#minecraft:gaze_disguise_equipment`.
